#!/usr/bin/env bash
create-jdbc-connection-pool --datasourceclassname com.mysql.jdbc.jdbc2.optional.MysqlDataSource --restype javax.sql.DataSource --property user=root:password=root:DatabaseName=alvin:ServerName=host.docker.internal:port=3306:url=jdbc\:mysql\://host.docker.internal\:3306/alvin alvin_pool
create-jdbc-resource --connectionpoolid alvin_pool jdbc/alvin

deploy deployments/trmt.war
