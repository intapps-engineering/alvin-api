CREATE TABLE `location_point` (
  `location_id` VARCHAR(48) NOT NULL,
  `id_prefix` INT NULL,
  `org_id` VARCHAR(48) NULL,
  `name` VARCHAR(45) NULL,
  `address_line_1` VARCHAR(150) NULL,
  `address_line_2` VARCHAR(150) NULL,
  `city` VARCHAR(45) NULL,
  `state_id` VARCHAR(45) NULL,
  `password` VARCHAR(100) NULL,
  PRIMARY KEY (`location_id`),
  INDEX `fk_location_org_id_idx` (`org_id` ASC),
  INDEX `fk_location_state_id_idx` (`state_id` ASC),
  CONSTRAINT `fk_location_org_id`
    FOREIGN KEY (`org_id`)
    REFERENCES `organisation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_state_id`
    FOREIGN KEY (`state_id`)
    REFERENCES `state` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);