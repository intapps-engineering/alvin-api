CREATE TABLE `user` (
  `email` VARCHAR(48) NOT NULL,
  `org_id` VARCHAR(45) NULL,
  `first_name` VARCHAR(100) NULL,
  `last_name` VARCHAR(100) NULL,
  `phone_number` VARCHAR(45) NULL,
  `password` VARCHAR(100) NULL,
  PRIMARY KEY (`email`),
  INDEX `fk_user_org_id_idx` (`org_id` ASC),
  CONSTRAINT `fk_user_org_id`
    FOREIGN KEY (`org_id`)
    REFERENCES `organisation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);