CREATE TABLE `organisation` (
  `id` VARCHAR(48) NOT NULL,
  `id_prefix` INT NULL,
  `name` VARCHAR(100) NULL,
  `industry_id` VARCHAR(45) NULL,
  `license_id` VARCHAR(100) NULL,
  `logo_url` VARCHAR(300) NULL,
  `date_created` DATE NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_org_industry_id_idx` (`industry_id` ASC),
  CONSTRAINT `fk_org_industry_id`
    FOREIGN KEY (`industry_id`)
    REFERENCES `industry` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);