CREATE TABLE `destination` (
  `id` varchar(48) NOT NULL,
  `org_id` varchar(48) DEFAULT NULL,
  `location_id` varchar(48) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `floor` varchar(32) DEFAULT NULL,
  `email_address` varchar(64) DEFAULT NULL,
  `phone_number` varchar(32) DEFAULT NULL,
  `date_added` DATETIME NULL DEFAULT NULL,
  `status` ENUM('ACTIVE', 'INACTIVE') NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;