/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.config;


import com.ia.tramta.service.OrganisationService;
import com.ia.tramta.service.UserService;
import com.ia.tramta.util.exception.GeneralAppExceptionMapper;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

@javax.ws.rs.ApplicationPath("/api")

public class ApplicationConfig extends Application{
    @Override    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        
        s.add(UserService.class);
        s.add(OrganisationService.class);
        s.add(GeneralAppExceptionMapper.class);
        
        return s;
    }
}
