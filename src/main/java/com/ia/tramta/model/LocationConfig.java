package com.ia.tramta.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "location_config")
@NamedQueries({
        @NamedQuery(name = "LocationConfig.findByLocationPointId", query = "SELECT lc FROM LocationConfig lc WHERE lc.locationId = :locationId")
})
public class LocationConfig {
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "location_id")
    private String locationId;
    @Basic
    @Column(name = "use_qr_code")
    private Boolean useQrCode;
    @Basic
    @Column(name = "collect_phone_number")
    private Boolean collectPhoneNumber;
    @Basic
    @Column(name = "collect_first_name")
    private Boolean collectFirstName;
    @Basic
    @Column(name = "collect_last_name")
    private Boolean collectLastName;
    @Basic
    @Column(name = "collect_email")
    private Boolean collectEmail;
    @Basic
    @Column(name = "collect_address")
    private Boolean collectAddress;
    @Basic
    @Column(name = "ask_for_destination")
    private Boolean askForDestination;
    @Basic
    @Column(name = "ask_for_number_of_persons")
    private Boolean askForNumberOfPersons;
    @Basic
    @Column(name = "notify_destination_via_email")
    private Boolean notifyDestinationViaEmail;
    @Basic
    @Column(name = "notify_destination_via_sms")
    private Boolean notifyDestinationViaSms;
    @Basic
    @Column(name = "disable_sign_in_button")
    private Boolean disableSignInButton;
    @Basic
    @Column(name = "disable_sign_out_button")
    private Boolean disableSignOutButton;

    public LocationConfig() {}

    public LocationConfig(boolean setupDefault) {
        if (setupDefault) {
            this.useQrCode = Boolean.FALSE;
            this.collectPhoneNumber = Boolean.TRUE;
            this.collectFirstName = Boolean.TRUE;
            this.collectLastName = Boolean.TRUE;
            this.collectEmail = Boolean.TRUE;
            this.collectAddress = Boolean.TRUE;
            this.askForDestination = Boolean.TRUE;
            this.askForNumberOfPersons = Boolean.TRUE;
            this.notifyDestinationViaEmail = Boolean.FALSE;
            this.notifyDestinationViaSms = Boolean.FALSE;
            this.disableSignInButton = Boolean.FALSE;
            this.disableSignOutButton = Boolean.FALSE;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public Boolean getUseQrCode() {
        return useQrCode;
    }

    public void setUseQrCode(Boolean useQrCode) {
        this.useQrCode = useQrCode;
    }

    public Boolean getCollectPhoneNumber() {
        return collectPhoneNumber;
    }

    public void setCollectPhoneNumber(Boolean collectPhoneNumber) {
        this.collectPhoneNumber = collectPhoneNumber;
    }

    public Boolean getCollectFirstName() {
        return collectFirstName;
    }

    public void setCollectFirstName(Boolean collectFirstName) {
        this.collectFirstName = collectFirstName;
    }

    public Boolean getCollectLastName() {
        return collectLastName;
    }

    public void setCollectLastName(Boolean collectLastName) {
        this.collectLastName = collectLastName;
    }

    public Boolean getCollectEmail() {
        return collectEmail;
    }

    public void setCollectEmail(Boolean collectEmail) {
        this.collectEmail = collectEmail;
    }

    public Boolean getCollectAddress() {
        return collectAddress;
    }

    public void setCollectAddress(Boolean collectAddress) {
        this.collectAddress = collectAddress;
    }

    public Boolean getAskForDestination() {
        return askForDestination;
    }

    public void setAskForDestination(Boolean askForDestination) {
        this.askForDestination = askForDestination;
    }

    public Boolean getAskForNumberOfPersons() {
        return askForNumberOfPersons;
    }

    public void setAskForNumberOfPersons(Boolean askForNumberOfPersons) {
        this.askForNumberOfPersons = askForNumberOfPersons;
    }

    public Boolean getNotifyDestinationViaEmail() {
        return notifyDestinationViaEmail;
    }

    public void setNotifyDestinationViaEmail(Boolean notifyDestinationViaEmail) {
        this.notifyDestinationViaEmail = notifyDestinationViaEmail;
    }

    public Boolean getNotifyDestinationViaSms() {
        return notifyDestinationViaSms;
    }

    public void setNotifyDestinationViaSms(Boolean notifyDestinationViaSms) {
        this.notifyDestinationViaSms = notifyDestinationViaSms;
    }

    public Boolean getDisableSignInButton() {
        return disableSignInButton;
    }

    public void setDisableSignInButton(Boolean disableSignInButton) {
        this.disableSignInButton = disableSignInButton;
    }

    public Boolean getDisableSignOutButton() {
        return disableSignOutButton;
    }

    public void setDisableSignOutButton(Boolean disableSignOutButton) {
        this.disableSignOutButton = disableSignOutButton;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationConfig that = (LocationConfig) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(locationId, that.locationId) &&
                Objects.equals(useQrCode, that.useQrCode) &&
                Objects.equals(collectPhoneNumber, that.collectPhoneNumber) &&
                Objects.equals(collectFirstName, that.collectFirstName) &&
                Objects.equals(collectLastName, that.collectLastName) &&
                Objects.equals(collectEmail, that.collectEmail) &&
                Objects.equals(collectAddress, that.collectAddress) &&
                Objects.equals(askForDestination, that.askForDestination) &&
                Objects.equals(askForNumberOfPersons, that.askForNumberOfPersons) &&
                Objects.equals(notifyDestinationViaEmail, that.notifyDestinationViaEmail) &&
                Objects.equals(notifyDestinationViaSms, that.notifyDestinationViaSms) &&
                Objects.equals(disableSignInButton, that.disableSignInButton) &&
                Objects.equals(disableSignOutButton, that.disableSignOutButton);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, locationId, useQrCode, collectPhoneNumber, collectFirstName, collectLastName, collectEmail, collectAddress, askForDestination, askForNumberOfPersons, notifyDestinationViaEmail, notifyDestinationViaSms, disableSignInButton, disableSignOutButton);
    }
}
