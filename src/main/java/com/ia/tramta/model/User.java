package com.ia.tramta.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "user")
@NamedQueries({
        @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email "),
        @NamedQuery(name = "User.findByFacebookEmail", query = "SELECT u FROM User u WHERE u.facebookEmail = :email "),
        @NamedQuery(name = "User.findByTwitterEmail", query = "SELECT u FROM User u WHERE u.twitterEmail = :email "),
        @NamedQuery(name = "User.findByUsernameFirstAndLastName", query = "SELECT u FROM User u WHERE u.email LIKE :email OR u.firstName LIKE :firstName OR u.lastName LIKE :lastName")})

public class User implements Serializable {
    private static final long serialVersionUID = -1842468098075739357L;

    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "org_id")
    private String orgId;

    @Basic
    @Column(name = "first_name")
    private String firstName;
    
    @Basic
    @Column(name = "last_name")
    private String lastName;
    
    @Basic
    @Column(name = "phone_number")
    private String phoneNumber;
    
    @Basic
    @Column(name = "password")
    private String password;

    @Column(name = "photo_url")
    private String photoUrl;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "facebook_email")
    private String facebookEmail;

    @Column(name = "twitter_email")
    private String twitterEmail;

    @Column(name = "oauth_uid")
    private String oauthUid;

    @Column(name = "oauth_provider")
    private Integer oauthProvider;

    @Column(name = "twitter_id")
    private String twitterId;
    
    @Column(name = "facebook_id")
    private String facebookId;
    
    @Column(name = "optout")
    private String optOut;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getFacebookEmail() {
        return facebookEmail;
    }

    public void setFacebookEmail(String facebookEmail) {
        this.facebookEmail = facebookEmail;
    }

    public String getTwitterEmail() {
        return twitterEmail;
    }

    public void setTwitterEmail(String twitterEmail) {
        this.twitterEmail = twitterEmail;
    }

    public String getOauthUid() {
        return oauthUid;
    }

    public void setOauthUid(String oauthUid) {
        this.oauthUid = oauthUid;
    }

    public Integer getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(Integer oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getOptOut() {
        return optOut;
    }

    public void setOptOut(String optOut) {
        this.optOut = optOut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(phoneNumber, user.phoneNumber) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {

        return Objects.hash(email, firstName, lastName, phoneNumber, password);
    }
}
