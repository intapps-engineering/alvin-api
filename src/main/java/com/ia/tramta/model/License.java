package com.ia.tramta.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "license")
@XmlRootElement
public class License implements Serializable {
    private static final long serialVersionUID = 1976800009386089640L;
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "alt_id")
    private String altId;
    @Basic
    @Column(name = "description")
    private String description;
    @Basic
    @Column(name = "class")
    private String klass;
    @Basic
    @Column(name = "period")
    private String period;
    @Basic
    @Column(name = "location_limit")
    private Integer locationLimit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAltId() {
        return altId;
    }

    public void setAltId(String altId) {
        this.altId = altId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKlass() {
        return klass;
    }

    public void setKlass(String klass) {
        this.klass = klass;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Integer getLocationLimit() {
        return locationLimit;
    }

    public void setLocationLimit(Integer locationLimit) {
        this.locationLimit = locationLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        License license = (License) o;
        return Objects.equals(id, license.id) &&
                Objects.equals(name, license.name) &&
                Objects.equals(altId, license.altId) &&
                Objects.equals(description, license.description) &&
                Objects.equals(klass, license.klass) &&
                Objects.equals(period, license.period) &&
                Objects.equals(locationLimit, license.locationLimit);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, altId, description, klass, period, locationLimit);
    }
}
