package com.ia.tramta.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "city")
@NamedQueries({
        @NamedQuery(name = "city.findByStateId", query = "SELECT c FROM City c WHERE c.stateId = :stateId")
})
public class City {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "state_id")
    private int stateId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return id == city.id &&
                stateId == city.stateId &&
                Objects.equals(name, city.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, stateId);
    }
}
