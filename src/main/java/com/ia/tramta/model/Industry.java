package com.ia.tramta.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "industry")
@XmlRootElement
public class Industry implements Serializable {
    private static final long serialVersionUID = -7099134465720565770L;
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "name")
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Industry industry = (Industry) o;
        return Objects.equals(id, industry.id) &&
                Objects.equals(name, industry.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }
}
