package com.ia.tramta.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "destination")
@NamedQueries({
        @NamedQuery(name = "Destination.findTotalCountByLocationId", query = "SELECT d FROM Destination d WHERE d.locationPointId = :locationId")
})
public class Destination implements Serializable {
    private static final long serialVersionUID = -1814634958509915896L;
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "org_id")
    private String orgId;
    @Basic
    @Column(name = "location_id")
    private String locationPointId;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "floor")
    private String floor;
    @Basic
    @Column(name = "email_address")
    private String emailAddress;
    @Basic
    @Column(name = "phone_number")
    private String phoneNumber;
    @Basic
    @Column(name = "date_added")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAdded;
    @Basic
    @Column(name = "status")
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getLocationPointId() {
        return locationPointId;
    }

    public void setLocationPointId(String locationPointId) {
        this.locationPointId = locationPointId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Destination that = (Destination) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(orgId, that.orgId) &&
                Objects.equals(locationPointId, that.locationPointId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(floor, that.floor) &&
                Objects.equals(emailAddress, that.emailAddress) &&
                Objects.equals(phoneNumber, that.phoneNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, orgId, locationPointId, name, floor, emailAddress, phoneNumber);
    }

}
