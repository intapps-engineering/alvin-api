package com.ia.tramta.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "organisation")
public class Organisation implements Serializable {
    private static final long serialVersionUID = 2178448701505452225L;
    @Id
    @Column(name = "id")
    private String id;
    @Basic
    @Column(name = "id_prefix")
    private Integer idPrefix;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "industry_id")
    private String industryId;
    @Basic
    @Column(name = "license_id")
    private String licenseId;
    @Basic
    @Column(name = "logo_url")
    private String logoUrl;
    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "date_created")
    private Date dateCreated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIdPrefix() {
        return idPrefix;
    }

    public void setIdPrefix(Integer idPrefix) {
        this.idPrefix = idPrefix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organisation that = (Organisation) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(idPrefix, that.idPrefix) &&
                Objects.equals(name, that.name) &&
                Objects.equals(logoUrl, that.logoUrl) &&
                Objects.equals(dateCreated, that.dateCreated);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, idPrefix, name, logoUrl, dateCreated);
    }
}
