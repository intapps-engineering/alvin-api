package com.ia.tramta.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "state")
@NamedQueries({
        @NamedQuery(name = "state.findByCountryId", query = "SELECT s FROM State s WHERE s.countryId = :countryId")
})
public class State {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "country_id")
    private int countryId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return id == state.id &&
                countryId == state.countryId &&
                Objects.equals(name, state.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, countryId);
    }
}
