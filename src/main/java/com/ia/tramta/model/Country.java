package com.ia.tramta.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "country")
public class Country {
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "sort_name")
    private String sortName;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "phone_code")
    private int phoneCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(int phoneCode) {
        this.phoneCode = phoneCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return id == country.id &&
                phoneCode == country.phoneCode &&
                Objects.equals(sortName, country.sortName) &&
                Objects.equals(name, country.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, sortName, name, phoneCode);
    }
}
