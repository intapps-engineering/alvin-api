package com.ia.tramta.pojo;

import java.io.Serializable;

public class UserOrgPayload implements Serializable {
    private static final long serialVersionUID = -4837766320269082215L;
    private AppOrganisation appOrganisation;
    private AppUser appUser;

    public UserOrgPayload() {}

    public AppOrganisation getAppOrganisation() {
        return appOrganisation;
    }

    public void setAppOrganisation(AppOrganisation appOrganisation) {
        this.appOrganisation = appOrganisation;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }
}
