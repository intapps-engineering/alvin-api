package com.ia.tramta.pojo;

import java.util.Date;
import java.util.Objects;

public class AppDestination {
   
    private String id;
    private String orgId;
    private String locationPointId;
    private String name;
    private String floor;
    private String emailAddress;
    private String phoneNumber;
    private String dateAdded;
    private Date dateAddedObject;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getLocationPointId() {
        return locationPointId;
    }

    public void setLocationPointId(String locationPointId) {
        this.locationPointId = locationPointId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateAddedObject() {
        return dateAddedObject;
    }

    public void setDateAddedObject(Date dateAddedObject) {
        this.dateAddedObject = dateAddedObject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppDestination that = (AppDestination) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(orgId, that.orgId) &&
                Objects.equals(locationPointId, that.locationPointId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(floor, that.floor) &&
                Objects.equals(emailAddress, that.emailAddress) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(dateAdded, that.dateAdded) &&
                Objects.equals(dateAddedObject, that.dateAddedObject);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, orgId, locationPointId, name, floor, emailAddress, phoneNumber, dateAdded, dateAddedObject);
    }
}
