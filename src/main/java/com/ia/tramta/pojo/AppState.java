package com.ia.tramta.pojo;

import java.util.Objects;

public class AppState {
    
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppState state = (AppState) o;
        return Objects.equals(id, state.id) &&
                Objects.equals(name, state.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }
}
