package com.ia.tramta.pojo;

import java.io.Serializable;

public class UserLocationPointPayload implements Serializable {
    private static final long serialVersionUID = 6048599431442234619L;
    private AppLocationPoint appLocationPoint;
    private AppUser appUser;
    private int totalCountOfOrgLocations;

    public UserLocationPointPayload() {}

    public AppLocationPoint getAppLocationPoint() {
        return appLocationPoint;
    }

    public void setAppLocationPoint(AppLocationPoint appLocationPoint) {
        this.appLocationPoint = appLocationPoint;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public int getTotalCountOfOrgLocations() {
        return totalCountOfOrgLocations;
    }

    public void setTotalCountOfOrgLocations(int totalCountOfOrgLocations) {
        this.totalCountOfOrgLocations = totalCountOfOrgLocations;
    }
}
