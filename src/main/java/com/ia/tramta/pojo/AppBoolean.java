/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.pojo;

import java.io.Serializable;

public class AppBoolean implements Serializable {
    private static final long serialVersionUID = -2117220815110666376L;
    Boolean state;
    
    public AppBoolean() {}
    
    public Boolean getStatus() {
        return state;
    }
    
    public void setStatus(Boolean state) {
        this.state = state;
    }

   
}
