package com.ia.tramta.pojo;

import java.util.List;

import com.ia.tramta.util.StatusType;

public class StatusTypePayload {

    private List<StatusType> statusTypes;

    public List<StatusType> getStatusTypes() {
        return statusTypes;
    }

    public void setStatusTypes(List<StatusType> statusTypes) {
        this.statusTypes = statusTypes;
    }
}
