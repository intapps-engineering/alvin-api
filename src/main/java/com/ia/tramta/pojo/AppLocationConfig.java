package com.ia.tramta.pojo;

import java.util.Objects;

public class AppLocationConfig {
    private String id;
    private String locationId;
    private Boolean useQrCode;
    private Boolean collectPhoneNumber;
    private Boolean collectFirstName;
    private Boolean collectLastName;
    private Boolean collectEmail;
    private Boolean collectAddress;
    private Boolean askForDestination;
    private Boolean askForNumberOfPersons;
    private Boolean notifyDestinationViaEmail;
    private Boolean notifyDestinationViaSms;
    private Boolean disableSignInButton;
    private Boolean disableSignOutButton;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public Boolean getUseQrCode() {
        return useQrCode;
    }

    public void setUseQrCode(Boolean useQrCode) {
        this.useQrCode = useQrCode;
    }

    public Boolean getCollectPhoneNumber() {
        return collectPhoneNumber;
    }

    public void setCollectPhoneNumber(Boolean collectPhoneNumber) {
        this.collectPhoneNumber = collectPhoneNumber;
    }

    public Boolean getCollectFirstName() {
        return collectFirstName;
    }

    public void setCollectFirstName(Boolean collectFirstName) {
        this.collectFirstName = collectFirstName;
    }

    public Boolean getCollectLastName() {
        return collectLastName;
    }

    public void setCollectLastName(Boolean collectLastName) {
        this.collectLastName = collectLastName;
    }

    public Boolean getCollectEmail() {
        return collectEmail;
    }

    public void setCollectEmail(Boolean collectEmail) {
        this.collectEmail = collectEmail;
    }

    public Boolean getCollectAddress() {
        return collectAddress;
    }

    public void setCollectAddress(Boolean collectAddress) {
        this.collectAddress = collectAddress;
    }

    public Boolean getAskForDestination() {
        return askForDestination;
    }

    public void setAskForDestination(Boolean askForDestination) {
        this.askForDestination = askForDestination;
    }

    public Boolean getAskForNumberOfPersons() {
        return askForNumberOfPersons;
    }

    public void setAskForNumberOfPersons(Boolean askForNumberOfPersons) {
        this.askForNumberOfPersons = askForNumberOfPersons;
    }

    public Boolean getNotifyDestinationViaEmail() {
        return notifyDestinationViaEmail;
    }

    public void setNotifyDestinationViaEmail(Boolean notifyDestinationViaEmail) {
        this.notifyDestinationViaEmail = notifyDestinationViaEmail;
    }

    public Boolean getNotifyDestinationViaSms() {
        return notifyDestinationViaSms;
    }

    public void setNotifyDestinationViaSms(Boolean notifyDestinationViaSms) {
        this.notifyDestinationViaSms = notifyDestinationViaSms;
    }

    public Boolean getDisableSignInButton() {
        return disableSignInButton;
    }

    public void setDisableSignInButton(Boolean disableSignInButton) {
        this.disableSignInButton = disableSignInButton;
    }

    public Boolean getDisableSignOutButton() {
        return disableSignOutButton;
    }

    public void setDisableSignOutButton(Boolean disableSignOutButton) {
        this.disableSignOutButton = disableSignOutButton;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppLocationConfig that = (AppLocationConfig) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(locationId, that.locationId) &&
                Objects.equals(useQrCode, that.useQrCode) &&
                Objects.equals(collectPhoneNumber, that.collectPhoneNumber) &&
                Objects.equals(collectFirstName, that.collectFirstName) &&
                Objects.equals(collectLastName, that.collectLastName) &&
                Objects.equals(collectEmail, that.collectEmail) &&
                Objects.equals(collectAddress, that.collectAddress) &&
                Objects.equals(askForDestination, that.askForDestination) &&
                Objects.equals(askForNumberOfPersons, that.askForNumberOfPersons) &&
                Objects.equals(notifyDestinationViaEmail, that.notifyDestinationViaEmail) &&
                Objects.equals(notifyDestinationViaSms, that.notifyDestinationViaSms) &&
                Objects.equals(disableSignInButton, that.disableSignInButton) &&
                Objects.equals(disableSignOutButton, that.disableSignOutButton);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, locationId, useQrCode, collectPhoneNumber, collectFirstName, collectLastName, collectEmail, collectAddress, askForDestination, askForNumberOfPersons, notifyDestinationViaEmail, notifyDestinationViaSms, disableSignInButton, disableSignOutButton);
    }
}
