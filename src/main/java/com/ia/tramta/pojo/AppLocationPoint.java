package com.ia.tramta.pojo;

import java.util.Date;
import java.util.Objects;

public class AppLocationPoint {

    private String id;
    private String idPrefix;
    private String orgId;
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String countryId;
    private String stateId;
    private String cityId;
    private String countryName;
    private String stateName;
    private String cityName;
    private String status;
    private String dateAdded;
    private Date dateAddedObject;
    private AppLocationConfig appLocationConfig;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPrefix() {
        return idPrefix;
    }

    public void setIdPrefix(String idPrefix) {
        this.idPrefix = idPrefix;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateAddedObject() {
        return dateAddedObject;
    }

    public void setDateAddedObject(Date dateAddedObject) {
        this.dateAddedObject = dateAddedObject;
    }

    public AppLocationConfig getAppLocationConfig() {
        return appLocationConfig;
    }

    public void setAppLocationConfig(AppLocationConfig appLocationConfig) {
        this.appLocationConfig = appLocationConfig;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppLocationPoint that = (AppLocationPoint) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(idPrefix, that.idPrefix) &&
                Objects.equals(name, that.name) &&
                Objects.equals(addressLine1, that.addressLine1) &&
                Objects.equals(addressLine2, that.addressLine2) &&
                Objects.equals(cityId, that.cityId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idPrefix, name, addressLine1, addressLine2, cityId);
    }
}
