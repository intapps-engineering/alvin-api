/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.model.Destination;
import com.ia.tramta.pojo.AppDestination;
import com.ia.tramta.util.exception.GeneralAppException;

public interface DestinationManagerLocal {

    AppDestination addDestination(String locationPointId, String orgId, String name, String floor, String emailAddress, String phoneNumber,
                                  String rawToken) throws GeneralAppException;

    AppDestination getAppDestination(Destination destination) throws GeneralAppException;

    AppDestination updateDestinationStatus(String destinationId, String statusTypeId, String rawToken) throws GeneralAppException;
}