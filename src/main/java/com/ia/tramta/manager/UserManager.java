/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.data.manager.UserDataManagerLocal;
import com.ia.tramta.model.User;
import com.ia.tramta.pojo.AppBoolean;
import com.ia.tramta.pojo.AppUser;
import com.ia.tramta.pojo.UserPayload;
import com.ia.tramta.util.CodeGenerator;
import com.ia.tramta.util.JWT;
import com.ia.tramta.util.MD5;
import com.ia.tramta.util.OnboardingStatusType;
import com.ia.tramta.util.SocialPlatformType;
import com.ia.tramta.util.Verifier;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.ia.tramta.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;

import java.util.ArrayList;

@Stateless
public class UserManager implements UserManagerLocal {

    @EJB
    private UserDataManagerLocal userDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;

    private final String USER_RESOURCE = "/user";
    private final String AUTHENTICATE_USER_RESOURCE = "/user/authenticate";
    private final String VERIFY_USERNAME_RESOURCE = "/user/verify";
    private final String CREATE_USERNAME_RESOURCE = "/user/username";
    private final String LINK_SOCIAL_ACCOUNT = "/user/link-social";
    private final String UNLINK_SOCIAL_ACCOUNT = "/user/unlink-social";
    private final long TOKEN_LIFETIME = 31556952000l;
    private final String DEFAULT_USER_AVATAR = "https://res.cloudinary.com/buls/image/upload/v1556371906/blank-profile-picture-973460_640.png";

          
    @Override
    public AppUser register(String email, String firstName, String lastName, String phoneNumber, String password) throws GeneralAppException {

        verifier.setResourceUrl(USER_RESOURCE)
                .verifyParams(email, firstName, lastName, phoneNumber, password);        
        
        if (!isValidEmailAddress(email)) {
            exceptionManager.throwInvalidEmailFormatException(USER_RESOURCE);
        }                
        
        if (userDataManager.get(email) != null) {
            exceptionManager.throwUserAlreadyExistException(USER_RESOURCE);
        }

        User user = new User();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPhoneNumber(phoneNumber);
        user.setCreationDate(Calendar.getInstance().getTime());
        user.setPassword(MD5.hash(password));
        user.setPhotoUrl(DEFAULT_USER_AVATAR);
        
        user = userDataManager.create(user);
        AppUser appUser = getAppUser(user);
        appUser = getAppUserWithToken(appUser);
           
        return appUser;
    }
    
    @Override
    public AppUser updateUserDetails(String email, String firstName, 
            String lastName, String phoneNumber, String photoUrl, String location, 
            String facebookEmail, String twitterEmail, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(USER_RESOURCE)
                .verifyParams(email, rawToken);
        
        verifier.setResourceUrl(USER_RESOURCE)
                .verifyJwt(rawToken);
        
        User user = userDataManager.get(email);
        
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_RESOURCE);
        }
        
        if (email != null && !email.isEmpty()) {
            user.setEmail(email);
        }
        if (firstName != null && !firstName.isEmpty()) {
            user.setFirstName(firstName);
        }
        if (lastName != null && !lastName.isEmpty()) {
            user.setLastName(lastName);
        }
        if (phoneNumber != null) {
            user.setPhoneNumber(phoneNumber);
        }
        if (photoUrl != null && !photoUrl.isEmpty()) {
            user.setPhotoUrl(photoUrl);
        } else {
            user.setPhotoUrl(DEFAULT_USER_AVATAR);
        }
        
        user.setFacebookEmail(facebookEmail);
        user.setTwitterEmail(twitterEmail);                 
    
        return getAppUser(userDataManager.update(user));
    }
    
    @Override
    public AppBoolean deleteUser(String username) throws GeneralAppException {

        verifier.setResourceUrl(USER_RESOURCE)
                .verifyParams(username);

        User user = userDataManager.get(username);
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_RESOURCE);
        }
        
        userDataManager.delete(user);
        
        return getAppBoolean(true);
    }
    
    @Override
    public AppUser getUserDetails (String username, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(USER_RESOURCE)
                .verifyParams(username, rawToken);
        
        verifier.setResourceUrl(USER_RESOURCE)
                .verifyJwt(rawToken);
        
        AppUser appUser = null;
        
        try {
            User user = userDataManager.get(username);
            if (user != null) {
                appUser = getAppUser(user);
            }
        } catch (Exception e) {
            exceptionManager.throwUserDoesNotExistException(USER_RESOURCE);
        }
        
        return appUser;
    }

    @Override
    public User getUser (String username) throws GeneralAppException {

        verifier.setResourceUrl(USER_RESOURCE)
                .verifyParams(username);
        User user = userDataManager.get(username);

        return user;

    }

    @Override
    public AppUser authenticate (String id, String password, String type) throws GeneralAppException {
        
        AppUser appUser = null;
        if (type.equals(SocialPlatformType.EMAIL.getDescription())) {
            verifier.setResourceUrl(AUTHENTICATE_USER_RESOURCE)
                    .verifyParams(id, password, type);
        } else {
            verifier.setResourceUrl(AUTHENTICATE_USER_RESOURCE)
                    .verifyParams(id, type);
        }
                
        if (isValidEmailAddress(id)) {
            if (type.equals(SocialPlatformType.FACEBOOK.getDescription())) {                                
                List<User> facebookUser = userDataManager.getByFacebookEmail(id);

                if (facebookUser.isEmpty()) {  
                    facebookUser = userDataManager.getByEmail(id);
                    if (facebookUser.isEmpty()) {  
                        User user = new User();
                        user.setFacebookEmail(id);
                        user.setCreationDate(Calendar.getInstance().getTime());
                        userDataManager.create(user);
                        facebookUser.add(user);
                    } else {
                        User existingUser = facebookUser.get(0);
                        existingUser.setFacebookEmail(id);
                        existingUser = userDataManager.update(existingUser);
                        appUser = getAppUserWithToken(getAppUser(existingUser));
                        appUser = getOnboardingStatus(appUser, SocialPlatformType.FACEBOOK);
                    }
                } else {
                    appUser = getAppUserWithToken(getAppUser(facebookUser.get(0)));
                    appUser = getOnboardingStatus(appUser, SocialPlatformType.FACEBOOK);
                }
                
                appUser = getAppUserWithToken(getAppUser(facebookUser.get(0)));
                appUser = getOnboardingStatus(appUser, SocialPlatformType.FACEBOOK);
            } else if (type.equals(SocialPlatformType.TWITTER.getDescription())) {                            
                List<User> twitterUser = userDataManager.getByTwitterEmail(id);

                if (twitterUser.isEmpty()) {                                     
                    twitterUser = userDataManager.getByEmail(id);
                    if (twitterUser.isEmpty()) {
                        User user = new User();
                        user.setTwitterEmail(id);
                        user.setCreationDate(Calendar.getInstance().getTime());
                        userDataManager.create(user);
                        twitterUser.add(user);
                    } else {
                        User existingUser = twitterUser.get(0);
                        existingUser.setTwitterEmail(id);
                        existingUser = userDataManager.update(existingUser);
                        appUser = getAppUserWithToken(getAppUser(existingUser));
                        appUser = getOnboardingStatus(appUser, SocialPlatformType.TWITTER);
                    }
                } else {
                    appUser = getAppUserWithToken(getAppUser(twitterUser.get(0)));
                    appUser = getOnboardingStatus(appUser, SocialPlatformType.TWITTER);
                } 

                appUser = getAppUserWithToken(getAppUser(twitterUser.get(0)));
                appUser = getOnboardingStatus(appUser, SocialPlatformType.TWITTER);
            } else if (type.equals(SocialPlatformType.EMAIL.getDescription())) {
                
                User emailUser = userDataManager.getByEmail(id).isEmpty() ? null : userDataManager.getByEmail(id).get(0);
                if (emailUser == null) {
                    exceptionManager.throwUserDoesNotExistException(AUTHENTICATE_USER_RESOURCE);
                } else {
                    if (!(emailUser.getPassword().equals(MD5.hash(password)))) {
                        exceptionManager.throwInvalidLoginCredentialsException(AUTHENTICATE_USER_RESOURCE);
                    } 
                    appUser = getAppUserWithToken(getAppUser(emailUser));
                    appUser = getOnboardingStatus(appUser, SocialPlatformType.EMAIL);
                }
            }
        } else {
            User usernameUser = userDataManager.get(id);
            
            if (usernameUser == null) {
                exceptionManager.throwUserDoesNotExistException(AUTHENTICATE_USER_RESOURCE);
            }
            
            if (usernameUser.getPassword() == null || usernameUser.getPassword().isEmpty()) {
                exceptionManager.throwNoEmailLoginAllowedException(AUTHENTICATE_USER_RESOURCE);
            }
            
            if (!(usernameUser != null && usernameUser.getPassword().equals(MD5.hash(password)))) {
                exceptionManager.throwInvalidLoginCredentialsException(AUTHENTICATE_USER_RESOURCE);
            }
            appUser = getAppUserWithToken(getAppUser(usernameUser));
            appUser = getOnboardingStatus(appUser, SocialPlatformType.EMAIL);
        } 
            
        
        
        return appUser;
    }
    
    @Override
    public AppBoolean verifyEmail(String email) throws GeneralAppException{
        verifier.setResourceUrl(VERIFY_USERNAME_RESOURCE)
                .verifyParams(email);
        
        User user = userDataManager.get(email);
        if (user == null ) {
            return getAppBoolean(true);
        }
        return getAppBoolean(false);
    }
    
    @Override
    public AppUser createUsername(String username, String email, String tempKey) throws GeneralAppException{        
                
        verifier.setResourceUrl(CREATE_USERNAME_RESOURCE)
                .verifyParams(username, email, tempKey);
                
        if (!isValidEmailAddress(email)) {
            exceptionManager.throwInvalidEmailFormatException(CREATE_USERNAME_RESOURCE);
        }                
        
        if (userDataManager.get(username) != null) {
            exceptionManager.throwUserAlreadyExistException(CREATE_USERNAME_RESOURCE);
        }
        
        User existingUser = userDataManager.get(tempKey); 
        
        if (existingUser == null) {
            exceptionManager.throwUserDoesNotExistException(CREATE_USERNAME_RESOURCE);
        }
        
        User user = new User();
        
        user.setFacebookEmail(existingUser.getFacebookEmail());
        user.setTwitterEmail(existingUser.getTwitterEmail());
        user.setCreationDate(existingUser.getCreationDate());     
        
        user = userDataManager.create(user);
        AppUser appUser = getAppUser(user);
        appUser = getAppUserWithToken(appUser);
        
        userDataManager.delete(existingUser);
        
        return appUser;

    }
    
    @Override
    public AppUser linkSocialAcount(String username, String platform, String email, String rawToken) throws GeneralAppException {
        
        User user = null;
        
        verifier.setResourceUrl(LINK_SOCIAL_ACCOUNT)
                .verifyParams(username, platform, email, rawToken);
        
        verifier.setResourceUrl(LINK_SOCIAL_ACCOUNT)
                .verifyJwt(rawToken);
        
        user = userDataManager.get(username);
                
        if ( user != null ) {
            if (platform.equals(SocialPlatformType.FACEBOOK.getDescription())) {
                user.setFacebookEmail(email);
            } else if (platform.equals(SocialPlatformType.TWITTER.getDescription())) {
                user.setTwitterEmail(email);
            } else {
                exceptionManager.throwInvalidSocialPlatform(LINK_SOCIAL_ACCOUNT);
            }
        
        } else {
            exceptionManager.throwUserDoesNotExistException(LINK_SOCIAL_ACCOUNT);
        }        
        return getAppUser(userDataManager.update(user));

    }
    
    @Override
    public AppUser unlinkSocialAcount(String username, String platform, String rawToken) throws GeneralAppException {
        
        User user = null;
        
        verifier.setResourceUrl(UNLINK_SOCIAL_ACCOUNT)
                .verifyParams(username, platform, rawToken);
        
        verifier.setResourceUrl(UNLINK_SOCIAL_ACCOUNT)
                .verifyJwt(rawToken);
        
        user = userDataManager.get(username);
                
        if ( user != null ) {
            if (platform.equals(SocialPlatformType.FACEBOOK.getDescription())) {
                user.setFacebookEmail(null);
            } else if (platform.equals(SocialPlatformType.TWITTER.getDescription())) {
                user.setTwitterEmail(null);
            } else {
                exceptionManager.throwInvalidSocialPlatform(UNLINK_SOCIAL_ACCOUNT);
            }
        
        } else {
            exceptionManager.throwUserDoesNotExistException(UNLINK_SOCIAL_ACCOUNT);
        }        
        return getAppUser(userDataManager.update(user));

    }

    @Override
    public AppUser getAppUser(User user) {
        AppUser appUser = new AppUser();
        if (user != null) {
            appUser.setEmail(user.getEmail() == null ? "" : user.getEmail());
            appUser.setFirstName(user.getFirstName() == null ? "" : user.getFirstName());
            appUser.setLastName(user.getLastName() == null ? "" : user.getLastName());
            appUser.setPhoneNumber(user.getPhoneNumber() == null ? "" : user.getPhoneNumber());
            appUser.setCreationDate(user.getCreationDate());
            appUser.setTwitterEmail(user.getTwitterEmail() == null ? "" : user.getTwitterEmail());
            appUser.setFacebookEmail(user.getFacebookEmail() == null ? "" : user.getFacebookEmail());
            appUser.setPhotoUrl(user.getPhotoUrl() == null ? "" : user.getPhotoUrl());
        }
        
        return appUser;
    }

    @Override
    public AppUser getAppUserFromAuthToken(String rawToken, String resourceUrl) throws GeneralAppException {
        Claims claims = verifier.setResourceUrl(resourceUrl).verifyJwt(rawToken);
        String username = claims.getSubject();
        User user = getUser(username);
        AppUser appUser = getAppUser(user);

        return appUser;
    }

    @Override
    public User getUserFromAuthToken(String rawToken, String resourceUrl) throws GeneralAppException {
        Claims claims = verifier.setResourceUrl(resourceUrl).verifyJwt(rawToken);
        String username = claims.getSubject();
        User user = getUser(username);

        return user;
    }

    private AppUser getAppUserWithToken(AppUser appUser) {
        JWT token = new JWT();      
        appUser.setAuthToken(token.createJWT(appUser, TOKEN_LIFETIME));
        return appUser;
    }
    
    private AppBoolean getAppBoolean(Boolean status) {
        AppBoolean appBoolean = new AppBoolean();
        
        appBoolean.setStatus(status);
        
        return appBoolean;
    }
            
    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    private AppUser getOnboardingStatus(AppUser appUser, SocialPlatformType socialPlatform) {
        //User user = null;
        appUser.setOnboardingStatus(OnboardingStatusType.STATUS_UNKNOWN.getDescription());
        if (socialPlatform.getDescription().equals(SocialPlatformType.FACEBOOK.getDescription())) {
            if (appUser.getFacebookEmail() != null && !appUser.getFacebookEmail().equals("")) {
                //user = userDataManager.getByFacebookEmail(appUser.getFacebookEmail()).get(0);                
            }
        } else if (socialPlatform.getDescription().equals(SocialPlatformType.TWITTER.getDescription())) {
            if (appUser.getTwitterEmail() != null && !appUser.getTwitterEmail().equals("")) {
                //user = userDataManager.getByTwitterEmail(appUser.getFacebookEmail()).get(0);                
            }
        } else if (socialPlatform.getDescription().equals(SocialPlatformType.EMAIL.getDescription())) {
            if (appUser.getEmail() != null && !appUser.getEmail().equals("")) {
                //user = userDataManager.getByEmail(appUser.getEmail()).get(0);                
            } else if (appUser.getEmail() != null && !appUser.getEmail().equals("")) {
                //user = userDataManager.get(appUser.getEmail());
            }
        }
        // check the user's onboarding status here and set appropriate values for onboardingstatus
        return appUser;
    }
    
    @Override
    public UserPayload getListOfUsers(String emails, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(USER_RESOURCE).verifyParams(emails, rawToken);
        verifier.setResourceUrl(USER_RESOURCE).verifyJwt(rawToken);
        
        List<AppUser> usersDetails = new ArrayList<AppUser>();
        System.out.println("EMAILS: " + emails);
        String[] listOfEmails = emails.split("\\s*,\\s*");
        for (int i = 0; i < listOfEmails.length; i++) {
            String email = listOfEmails[i];
            if (email != null && !email.isEmpty()) {
                AppUser appUser = getUserDetails(email, rawToken);
                if (appUser != null) {
                    System.out.println("ADDING EMAIL: " + appUser.getEmail());
                    usersDetails.add(appUser);
                }
            }
        }
        
        UserPayload users = new UserPayload();
        users.setPageNumber(1);
        users.setPageSize(listOfEmails.length);
        users.setUsers(usersDetails);
        
        return users;
    }        

    @Override
    public UserPayload searchUsers(String searchTerm, String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User updateUser(User user) throws GeneralAppException {
        return userDataManager.update(user);
    }

}
