/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.model.User;
import com.ia.tramta.pojo.AppBoolean;
import com.ia.tramta.pojo.AppUser;
import com.ia.tramta.pojo.UserPayload;
import com.ia.tramta.util.exception.GeneralAppException;

public interface UserManagerLocal {
 
    
    AppUser getAppUser (User user);

    AppUser getAppUserFromAuthToken (String rawToken, String reseourceUrl) throws GeneralAppException;

    User getUserFromAuthToken (String rawToken, String reseourceUrl) throws GeneralAppException;
    
    AppUser getUserDetails (String username, String rawToken) throws GeneralAppException;

    User getUser (String username) throws GeneralAppException;

    AppUser register(String email, String firstName, String lastName, String phoneNumber, String password) throws GeneralAppException;
    
    AppUser updateUserDetails(String email, String firstName, String lastName,
                                String phoneNumber, String photoUrl, String location, String facebookEmail,
                                String twitterEmail, String rawToken) throws GeneralAppException;
    
    AppBoolean deleteUser(String username) throws GeneralAppException;
    
    AppUser authenticate (String id, String password, String type) throws GeneralAppException;
    
    AppBoolean verifyEmail(String email) throws GeneralAppException;
            
    AppUser createUsername(String username, String email, String tempKey) throws GeneralAppException;
    
    AppUser linkSocialAcount(String username, String platform, String email, String rawToken) throws GeneralAppException;
    
    AppUser unlinkSocialAcount(String username, String platform, String rawToken) throws GeneralAppException;
    
    UserPayload getListOfUsers (String usernames, String rawToken) throws GeneralAppException;
            
    UserPayload searchUsers(String searchTerm, String pageNumber, String pageSize, String rawToken) throws GeneralAppException;

    User updateUser(User user) throws GeneralAppException;
}