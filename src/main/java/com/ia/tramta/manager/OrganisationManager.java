/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.data.manager.OrganisationDataManagerLocal;
import com.ia.tramta.model.Organisation;
import com.ia.tramta.model.User;
import com.ia.tramta.pojo.AppOrganisation;
import com.ia.tramta.util.*;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;

@Stateless
public class OrganisationManager implements OrganisationManagerLocal {

    @EJB
    private OrganisationDataManagerLocal organisationDataManager;

    @EJB
    private UserManagerLocal userManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;

    private final String ORG_LINK = "/org";
    private final String DEFAULT_ORG_AVATAR = "https://res.cloudinary.com/buls/image/upload/v1556371906/blank-profile-picture-973460_640.png";


    @Override
    public AppOrganisation createOrganisation(String name, String industryId, String logoUrl, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(ORG_LINK).verifyParams(name, industryId, logoUrl, rawToken);

        User organisationContact = userManager.getUserFromAuthToken(rawToken, ORG_LINK);
        if (organisationContact == null) {
            exceptionManager.throwUserDoesNotExistException(ORG_LINK);
        }

        Integer totalCountOfOrgs = organisationDataManager.getTotalCount();
        Organisation organisation = new Organisation();
        organisation.setId(codeGenerator.getToken());
        organisation.setIdPrefix(totalCountOfOrgs++);
        organisation.setName(name);
        organisation.setIndustryId(industryId);
        organisation.setLogoUrl(logoUrl == null ? DEFAULT_ORG_AVATAR : logoUrl);
        organisation.setDateCreated(new Date());
        organisation.setLicenseId(LicenseClassType.STARTER_TRIAL.getDescription());

        organisation = organisationDataManager.create(organisation);

        organisationContact.setOrgId(organisation.getId());
        userManager.updateUser(organisationContact);

        AppOrganisation appOrganisation = getAppOrganisation(organisation);
        //AppUser appOrganisationContact = userManager.getAppUser(organisationContact);
        
        return appOrganisation;
    }

    @Override
    public AppOrganisation getAppOrganisation(Organisation organisation) throws GeneralAppException {
        AppOrganisation appOrganisation = new AppOrganisation();
        if (organisation != null) {
            appOrganisation.setId(organisation.getId());
            appOrganisation.setIdPrefix(organisation.getIdPrefix());
            appOrganisation.setName(organisation.getName());
            appOrganisation.setIndustryId(organisation.getIndustryId());
            appOrganisation.setLogoUrl(organisation.getLogoUrl());
            appOrganisation.setDateCreated(organisation.getDateCreated());
            appOrganisation.setLicenseId(organisation.getLicenseId());
        }

        return appOrganisation;
    }

    @Override
    public boolean organisationExists(String orgId) throws GeneralAppException {
        Organisation organisation = organisationDataManager.get(orgId);
        if (organisation != null) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public Organisation getOrganisation(String orgId) throws GeneralAppException {
        return organisationDataManager.get(orgId);
    }
}
