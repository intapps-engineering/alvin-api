/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.model.LocationConfig;
import com.ia.tramta.pojo.AppLocationConfig;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.Local;


@Local
public interface LocationConfigManagerLocal {
    
    AppLocationConfig getLocationConfig(String locationPointId, String rawToken) throws GeneralAppException;

    AppLocationConfig updateLocationConfig(String id, String useQrCode, String collectPhoneNumber, String collectFirstName,
                                           String collectLastName, String collectEmail, String collectAddress, String askForDestination,
                                           String askForNumberOfPersons, String notifyDestinationViaEmail, String notifyDestinationViaSms,
                                           String disableSignInButton, String disableSignOutButton, String rawToken) throws GeneralAppException;

    AppLocationConfig create(LocationConfig locationConfig) throws GeneralAppException;
}
