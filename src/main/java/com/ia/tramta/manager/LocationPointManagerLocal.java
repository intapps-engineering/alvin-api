/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.model.LocationPoint;
import com.ia.tramta.pojo.AppLocationPoint;
import com.ia.tramta.util.exception.GeneralAppException;

public interface LocationPointManagerLocal {

    AppLocationPoint addLocationPoint(String orgId, String name, String addressLine1, String addressLine2, String countryId,
                                      String stateId, String cityId, String password, String rawToken) throws GeneralAppException;

    AppLocationPoint getAppLocationPoint(LocationPoint locationPoint) throws GeneralAppException;

    LocationPoint getLocationPoint(String locationPointId) throws GeneralAppException;

    AppLocationPoint updateLocationPointStatus(String locationPointId, String statusTypeId, String rawToken) throws GeneralAppException;
}