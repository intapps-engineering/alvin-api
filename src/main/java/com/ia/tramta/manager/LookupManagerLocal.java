/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.model.City;
import com.ia.tramta.model.Country;
import com.ia.tramta.model.State;
import com.ia.tramta.pojo.StatusTypePayload;
import com.ia.tramta.util.exception.GeneralAppException;

import java.util.List;

public interface LookupManagerLocal {


    Country getCountry(Integer countryId) throws GeneralAppException;

    State getState(Integer stateId) throws GeneralAppException;

    City getCity(Integer cityId) throws GeneralAppException;

    List<Country> getAllCountries(String rawToken) throws GeneralAppException;

    List<State> getAllStates(Integer countryId, String rawToken) throws GeneralAppException;

    List<City> getAllCities(Integer stateId, String rawToken) throws GeneralAppException;

    StatusTypePayload getAllStatusTypes(String rawToken) throws GeneralAppException;
}