/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.model.Organisation;
import com.ia.tramta.pojo.AppOrganisation;
import com.ia.tramta.util.exception.GeneralAppException;

public interface OrganisationManagerLocal {

    AppOrganisation createOrganisation(String name, String industryId, String logoUrl, String authToken) throws GeneralAppException;

    AppOrganisation getAppOrganisation(Organisation organisation) throws GeneralAppException;

    boolean organisationExists(String orgId) throws GeneralAppException;

    Organisation getOrganisation(String orgId) throws GeneralAppException;
}