/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.data.manager.LocationConfigDataManagerLocal;
import com.ia.tramta.model.LocationConfig;
import com.ia.tramta.pojo.AppLocationConfig;
import com.ia.tramta.util.Verifier;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class LocationConfigManager implements LocationConfigManagerLocal {

    @EJB
    private LocationConfigDataManagerLocal locationConfigDataManager;

    @EJB
    private Verifier verifier;

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    private String LOCATION_CONFIG_RESOURCE = "/org/location-point/location-config";

    @Override
    public AppLocationConfig getLocationConfig(String locationPointId, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(LOCATION_CONFIG_RESOURCE)
                .verifyParams(locationPointId)
                .verifyJwt(rawToken);

        AppLocationConfig appLocationConfig = new AppLocationConfig();
        List<LocationConfig> locationConfigs = locationConfigDataManager.getLocationConfigByLocationPointId(locationPointId);
        if (locationConfigs.size() > 0) {
            LocationConfig locationConfig = locationConfigs.get(0);
            appLocationConfig = getAppLocationConfig(locationConfig);
        }

        return appLocationConfig;
    }

    @Override
    public AppLocationConfig updateLocationConfig(String id, String useQrCode, String collectPhoneNumber,
                                                  String collectFirstName, String collectLastName, String collectEmail,
                                                  String collectAddress, String askForDestination, String askForNumberOfPersons,
                                                  String notifyDestinationViaEmail, String notifyDestinationViaSms,
                                                  String disableSignInButton, String disableSignOutButton,
                                                  String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(LOCATION_CONFIG_RESOURCE)
                .verifyParams(id)
                .verifyJwt(rawToken);

        LocationConfig locationConfig = locationConfigDataManager.getLocationConfig(id);
        if (locationConfig == null) {
            exceptionManager.throwLocationConfigNotExistException(LOCATION_CONFIG_RESOURCE);
        }

        locationConfig.setAskForDestination(toBoolean(askForDestination) == null ? locationConfig.getAskForDestination() : toBoolean(askForDestination));
        locationConfig.setAskForNumberOfPersons(toBoolean(askForNumberOfPersons) == null ? locationConfig.getAskForNumberOfPersons() : toBoolean(askForNumberOfPersons));
        locationConfig.setCollectAddress(toBoolean(collectAddress) == null ? locationConfig.getCollectAddress() : toBoolean(collectAddress));
        locationConfig.setCollectEmail(toBoolean(collectEmail) == null ? locationConfig.getCollectEmail() : toBoolean(collectEmail));
        locationConfig.setCollectFirstName(toBoolean(collectFirstName) == null ? locationConfig.getCollectFirstName() : toBoolean(collectFirstName));
        locationConfig.setCollectLastName(toBoolean(collectLastName) == null ? locationConfig.getCollectLastName() : toBoolean(collectLastName));
        locationConfig.setCollectPhoneNumber(toBoolean(collectPhoneNumber) == null ? locationConfig.getCollectPhoneNumber() : toBoolean(collectPhoneNumber));
        locationConfig.setDisableSignInButton(toBoolean(disableSignInButton) == null ? locationConfig.getDisableSignInButton() : toBoolean(disableSignInButton));
        locationConfig.setDisableSignOutButton(toBoolean(disableSignOutButton) == null ? locationConfig.getDisableSignOutButton() : toBoolean(disableSignOutButton));
        locationConfig.setNotifyDestinationViaEmail(toBoolean(notifyDestinationViaEmail) == null ? locationConfig.getNotifyDestinationViaEmail() : toBoolean(notifyDestinationViaEmail));
        locationConfig.setNotifyDestinationViaSms(toBoolean(notifyDestinationViaSms) == null ? locationConfig.getNotifyDestinationViaSms() : toBoolean(notifyDestinationViaSms));

        locationConfig = locationConfigDataManager.updateLocationConfig(locationConfig);
        AppLocationConfig appLocationConfig = getAppLocationConfig(locationConfig);

        return appLocationConfig;
    }

    @Override
    public AppLocationConfig create(LocationConfig locationConfig) throws GeneralAppException {
        locationConfig = locationConfigDataManager.create(locationConfig);
        AppLocationConfig appLocationConfig = getAppLocationConfig(locationConfig);

        return appLocationConfig;
    }

    private AppLocationConfig getAppLocationConfig(LocationConfig locationConfig) {
        AppLocationConfig appLocationConfig = new AppLocationConfig();

        if (locationConfig != null) {
            appLocationConfig.setId(locationConfig.getId());
            appLocationConfig.setLocationId(locationConfig.getLocationId());
            appLocationConfig.setAskForDestination(locationConfig.getAskForDestination());
            appLocationConfig.setAskForNumberOfPersons(locationConfig.getAskForNumberOfPersons());
            appLocationConfig.setCollectAddress(locationConfig.getCollectAddress());
            appLocationConfig.setCollectEmail(locationConfig.getCollectEmail());
            appLocationConfig.setCollectFirstName(locationConfig.getCollectFirstName());
            appLocationConfig.setCollectLastName(locationConfig.getCollectLastName());
            appLocationConfig.setCollectPhoneNumber(locationConfig.getCollectPhoneNumber());
            appLocationConfig.setDisableSignInButton(locationConfig.getDisableSignInButton());
            appLocationConfig.setDisableSignOutButton(locationConfig.getDisableSignOutButton());
            appLocationConfig.setNotifyDestinationViaEmail(locationConfig.getNotifyDestinationViaEmail());
            appLocationConfig.setNotifyDestinationViaSms(locationConfig.getNotifyDestinationViaSms());
            appLocationConfig.setUseQrCode(locationConfig.getUseQrCode());
        }

        return appLocationConfig;
    }


    private Boolean toBoolean(String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        Boolean booleanValue = Boolean.parseBoolean(value);

        return booleanValue;
    }
}

