/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.data.manager.CityDataManagerLocal;
import com.ia.tramta.data.manager.CountryDataManagerLocal;
import com.ia.tramta.data.manager.StateDataManagerLocal;
import com.ia.tramta.model.City;
import com.ia.tramta.model.Country;
import com.ia.tramta.model.State;
import com.ia.tramta.pojo.StatusTypePayload;
import com.ia.tramta.util.*;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class LookupManager implements LookupManagerLocal {

    @EJB
    private CountryDataManagerLocal countryDataManager;

    @EJB
    private StateDataManagerLocal stateDataManager;

    @EJB
    private CityDataManagerLocal cityDataManager;

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private Verifier verifier;

    private final String LOOKUP_RESOURCE = "/lookup";
    private final String COUNTRY_RESOURCE = LOOKUP_RESOURCE + "/country";
    private final String STATE_RESOURCE = LOOKUP_RESOURCE + "/state";
    private final String CITY_RESOURCE = LOOKUP_RESOURCE + "/city";
    private final String STATUS_TYPE_RESOURCE = LOOKUP_RESOURCE + "/status-type";


    @Override
    public Country getCountry(Integer countryId) throws GeneralAppException {
        verifier.setResourceUrl(COUNTRY_RESOURCE).verifyInteger(countryId);

        return countryDataManager.get(countryId);
    }

    @Override
    public State getState(Integer stateId) throws GeneralAppException {
        verifier.setResourceUrl(STATE_RESOURCE).verifyInteger(stateId);

        return stateDataManager.get(stateId);
    }

    @Override
    public City getCity(Integer cityId) throws GeneralAppException {
        verifier.setResourceUrl(CITY_RESOURCE).verifyInteger(cityId);

        return cityDataManager.get(cityId);
    }

    @Override
    public List<Country> getAllCountries(String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(COUNTRY_RESOURCE).verifyParams(rawToken);

        return countryDataManager.getAll();
    }

    @Override
    public List<State> getAllStates(Integer countryId, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(STATE_RESOURCE).verifyParams(rawToken);
        verifier.setResourceUrl(STATE_RESOURCE).verifyInteger(countryId);

        return stateDataManager.getAll(countryId);
    }

    @Override
    public List<City> getAllCities(Integer stateId, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(CITY_RESOURCE).verifyParams(rawToken);
        verifier.setResourceUrl(CITY_RESOURCE).verifyInteger(stateId);

        return cityDataManager.getAll(stateId);
    }

    @Override
    public StatusTypePayload getAllStatusTypes(String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(STATUS_TYPE_RESOURCE).verifyParams(rawToken);

        List<StatusType> statusTypes = new ArrayList<>();
        statusTypes.add(StatusType.ACTIVE);
        statusTypes.add(StatusType.INACTIVE);

        StatusTypePayload statusTypePayload = new StatusTypePayload();
        statusTypePayload.setStatusTypes(statusTypes);

        return statusTypePayload;
    }
}
