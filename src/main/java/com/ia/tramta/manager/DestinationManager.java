/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.data.manager.DestinationDataManagerLocal;
import com.ia.tramta.model.*;
import com.ia.tramta.pojo.AppDestination;
import com.ia.tramta.pojo.AppUser;
import com.ia.tramta.util.*;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;

@Stateless
public class DestinationManager implements DestinationManagerLocal {


    @EJB
    private DestinationDataManagerLocal destinationDataManager;

    @EJB
    private OrganisationManagerLocal organisationManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private UserManagerLocal userManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;

    @EJB
    private LocationPointManagerLocal locationManager;

    private final String DESTINATION_RESOURCE = "/org/location-point/destination";

    @Override
    public AppDestination addDestination(String locationPointId, String orgId, String name, String floor, String emailAddress, String phoneNumber,
                                         String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(DESTINATION_RESOURCE).verifyParams(locationPointId, orgId, name, rawToken);
        verifier.setResourceUrl(DESTINATION_RESOURCE).verifyJwt(rawToken);

        AppUser appUser = userManager.getAppUserFromAuthToken(rawToken, DESTINATION_RESOURCE);
        if (appUser == null) {
            exceptionManager.throwUserDoesNotExistException(DESTINATION_RESOURCE);
        }

        Organisation organisation = organisationManager.getOrganisation(orgId);
        if (organisation == null) {
            exceptionManager.throwOrganisationDoesNotExistException(DESTINATION_RESOURCE);
        }

        LocationPoint locationPoint = locationManager.getLocationPoint(locationPointId);
        if (locationPoint == null) {
            exceptionManager.throwLocationPointDoesNotExistException(DESTINATION_RESOURCE);
        }

        Destination destination = new Destination();
        destination.setId(codeGenerator.getToken());
        destination.setLocationPointId(locationPointId);
        destination.setOrgId(orgId);
        destination.setName(name);
        destination.setFloor(floor);
        destination.setEmailAddress(emailAddress);
        destination.setPhoneNumber(phoneNumber);
        destination.setDateAdded(new Date());
        destination.setStatus(StatusType.ACTIVE.getDescription());

        destination = destinationDataManager.create(destination);
        AppDestination appDestination = getAppDestination(destination);

        return appDestination;
    }

    @Override
    public AppDestination getAppDestination(Destination destination) throws GeneralAppException {
        AppDestination appDestination = new AppDestination();
        if (destination != null) {
            appDestination.setId(destination.getId());
            appDestination.setLocationPointId(destination.getLocationPointId());
            appDestination.setOrgId(destination.getOrgId());
            appDestination.setName(destination.getName());
            appDestination.setFloor(destination.getFloor());
            appDestination.setEmailAddress(destination.getEmailAddress());
            appDestination.setPhoneNumber(destination.getPhoneNumber());
            appDestination.setDateAdded(DateHandler.formatDate(destination.getDateAdded()));
            appDestination.setDateAddedObject(destination.getDateAdded());
            appDestination.setStatus(destination.getStatus());
        }
        
        return appDestination;
    }

    @Override
    public AppDestination updateDestinationStatus(String destinationId, String statusTypeId, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(DESTINATION_RESOURCE).verifyParams(destinationId, statusTypeId, rawToken)
                .verifyJwt(rawToken);

        Destination destination = destinationDataManager.get(destinationId);
        if (destination == null) {
            exceptionManager.throwDestinationDoesNotExistException(DESTINATION_RESOURCE);
        }

        String newStatus = StatusType.getStatusType(statusTypeId);

        if (newStatus == null) {
            exceptionManager.throwInvalidStatusTypeException(DESTINATION_RESOURCE);
        }

        destination.setStatus(newStatus);
        destinationDataManager.update(destination);

        AppDestination appDestination = getAppDestination(destination);

        return appDestination;
    }

}
