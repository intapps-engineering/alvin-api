/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.manager;

import com.ia.tramta.data.manager.LocationPointDataManagerLocal;
import com.ia.tramta.model.*;
import com.ia.tramta.pojo.AppLocationConfig;
import com.ia.tramta.pojo.AppLocationPoint;
import com.ia.tramta.pojo.AppUser;
import com.ia.tramta.util.*;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;

@Stateless
public class LocationPointManager implements LocationPointManagerLocal {


    @EJB
    private LocationPointDataManagerLocal locationPointDataManager;

    @EJB
    private OrganisationManagerLocal organisationManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private UserManagerLocal userManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;

    @EJB
    private LookupManagerLocal lookupManager;

    @EJB
    private LocationConfigManagerLocal locationConfigManager;

    private final String LOCATION_POINT_RESOURCE = "/org/location-point";

    @Override
    public AppLocationPoint addLocationPoint(String orgId, String name, String addressLine1, String addressLine2, String countryId,
                                             String stateId, String cityId, String password, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(LOCATION_POINT_RESOURCE).verifyParams(orgId, name, addressLine1, countryId, stateId, cityId, password, rawToken);
        verifier.setResourceUrl(LOCATION_POINT_RESOURCE).verifyInteger(countryId, stateId, cityId);
        verifier.setResourceUrl(LOCATION_POINT_RESOURCE).verifyJwt(rawToken);

        AppUser appUser = userManager.getAppUserFromAuthToken(rawToken, LOCATION_POINT_RESOURCE);
        if (appUser == null) {
            exceptionManager.throwUserDoesNotExistException(LOCATION_POINT_RESOURCE);
        }

        Organisation organisation = organisationManager.getOrganisation(orgId);
        if (organisation == null) {
            exceptionManager.throwOrganisationDoesNotExistException(LOCATION_POINT_RESOURCE);
        }

        Integer totalCountOfOrgLocations = locationPointDataManager.getTotalLocationPointCountInOrg(orgId);
        ++totalCountOfOrgLocations;
        String locationPointPrefix = totalCountOfOrgLocations.toString() + "-" + organisation.getIdPrefix().toString();
        LocationPoint locationPoint = new LocationPoint();
        locationPoint.setId(codeGenerator.getToken());
        locationPoint.setIdPrefix(locationPointPrefix);
        locationPoint.setOrgId(orgId);
        locationPoint.setName(name);
        locationPoint.setAddressLine1(addressLine1);
        locationPoint.setAddressLine2(addressLine2 == null ? "" : addressLine2);
        locationPoint.setCountryId(Integer.parseInt(countryId));
        locationPoint.setStateId(Integer.parseInt(stateId));
        locationPoint.setCityId(Integer.parseInt(cityId));
        locationPoint.setPassword(MD5.hash(password));
        locationPoint.setStatus(StatusType.ACTIVE.getDescription());
        locationPoint.setDateAdded(new Date());

        locationPoint = locationPointDataManager.create(locationPoint);
        AppLocationPoint appLocationPoint = getAppLocationPoint(locationPoint);

        LocationConfig locationConfig = new LocationConfig(Boolean.TRUE);
        locationConfig.setId(codeGenerator.getToken());
        locationConfig.setLocationId(locationPoint.getId());
        AppLocationConfig appLocationConfig = locationConfigManager.create(locationConfig);
        appLocationPoint.setAppLocationConfig(appLocationConfig);

        return appLocationPoint;
    }

    @Override
    public AppLocationPoint getAppLocationPoint(LocationPoint locationPoint) throws GeneralAppException {
        AppLocationPoint appLocationPoint = new AppLocationPoint();
        if (locationPoint != null) {
            appLocationPoint.setId(locationPoint.getId());
            appLocationPoint.setIdPrefix(locationPoint.getIdPrefix());
            appLocationPoint.setOrgId(locationPoint.getOrgId());
            appLocationPoint.setName(locationPoint.getName());
            appLocationPoint.setAddressLine1(locationPoint.getAddressLine1());
            appLocationPoint.setAddressLine2(locationPoint.getAddressLine2());
            appLocationPoint.setStatus(locationPoint.getStatus());
            appLocationPoint.setDateAdded(DateHandler.formatDate(locationPoint.getDateAdded()));
            appLocationPoint.setDateAddedObject(locationPoint.getDateAdded());

            Country country = lookupManager.getCountry(locationPoint.getCountryId());
            State state = lookupManager.getState(locationPoint.getStateId());
            City city = lookupManager.getCity(locationPoint.getCityId());

            if (country != null) {
                appLocationPoint.setCountryId(((Integer)country.getId()).toString());
                appLocationPoint.setCountryName(country.getName());
            }

            if (state != null) {
                appLocationPoint.setStateId(((Integer)state.getId()).toString());
                appLocationPoint.setStateName(state.getName());
            }

            if (city != null) {
                appLocationPoint.setCityId(((Integer)city.getId()).toString());
                appLocationPoint.setCityName(city.getName());
            }

        }
        
        return appLocationPoint;
    }

    @Override
    public LocationPoint getLocationPoint(String locationPointId) throws GeneralAppException {
        return locationPointDataManager.get(locationPointId);
    }

    @Override
    public AppLocationPoint updateLocationPointStatus(String locationPointId, String statusTypeId, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(LOCATION_POINT_RESOURCE).verifyParams(locationPointId, statusTypeId, rawToken)
                .verifyJwt(rawToken);

        LocationPoint locationPoint = locationPointDataManager.get(locationPointId);
        if (locationPoint == null) {
            exceptionManager.throwDestinationDoesNotExistException(LOCATION_POINT_RESOURCE);
        }

        String newStatus = StatusType.getStatusType(statusTypeId);
        if (newStatus == null) {
            exceptionManager.throwInvalidStatusTypeException(LOCATION_POINT_RESOURCE);
        }

        locationPoint.setStatus(newStatus);
        locationPointDataManager.update(locationPoint);

        AppLocationPoint appLocationPoint = getAppLocationPoint(locationPoint);

        return appLocationPoint;
    }

}
