/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.service;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ia.tramta.manager.DestinationManagerLocal;
import com.ia.tramta.manager.LocationConfigManagerLocal;
import com.ia.tramta.manager.LocationPointManagerLocal;
import com.ia.tramta.manager.LookupManagerLocal;
import com.ia.tramta.manager.OrganisationManagerLocal;
import com.ia.tramta.pojo.AppDestination;
import com.ia.tramta.pojo.AppLocationConfig;
import com.ia.tramta.pojo.AppLocationPoint;
import com.ia.tramta.pojo.AppOrganisation;
import com.ia.tramta.pojo.StatusTypePayload;
import com.ia.tramta.util.exception.GeneralAppException;



@Stateless
@Path("/v1/org")
public class OrganisationService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB
    OrganisationManagerLocal organisationManager;

    @EJB
    LocationPointManagerLocal locationPointManager;

    @EJB
    DestinationManagerLocal destinationManager;

    @EJB
    LookupManagerLocal lookupManager;

    @EJB
    LocationConfigManagerLocal locationConfigManager;
       
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createOrganisation(@QueryParam("name") String name,
                                      @QueryParam("industry-id") String industryId,
                                      @QueryParam("logo-url") String logoUrl,
                                      @HeaderParam("Authorization") String rawToken) throws GeneralAppException {
        
        AppOrganisation appOrganisation = organisationManager.createOrganisation(name, industryId, logoUrl, rawToken);

        return Response.ok(appOrganisation).build();
    }

    @POST
    @Path("/{org-id}/location-point")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addOrganisationLocationPoint(@PathParam("org-id") String orgId,
                                                 @QueryParam("name") String name,
                                                 @QueryParam("address-line-1") String addressLine1,
                                                 @QueryParam("address-line-2") String addressLine2,
                                                 @QueryParam("country-id") String countryId,
                                                 @QueryParam("state-id") String stateId,
                                                 @QueryParam("city-id") String cityId,
                                                 @QueryParam("password") String password,
                                                 @HeaderParam("Authorization") String rawToken) throws GeneralAppException {

        AppLocationPoint appLocationPoint = locationPointManager.addLocationPoint(orgId, name, addressLine1, addressLine2,
                countryId, stateId, cityId, password, rawToken);

        return Response.ok(appLocationPoint).build();
    }

    @POST
    @Path("/{org-id}/{location-point-id}/destination")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addLocationPointDestination(@PathParam("location-point-id") String locationPointId,
                                                @PathParam("org-id") String orgId,
                                                @QueryParam("name") String name,
                                                @QueryParam("floor") String floor,
                                                @QueryParam("email-address") String emailAddress,
                                                @QueryParam("phone-number") String phoneNumber,
                                                @HeaderParam("Authorization") String rawToken) throws GeneralAppException {

        AppDestination appDestination = destinationManager.addDestination(locationPointId, orgId, name, floor, emailAddress,
                phoneNumber, rawToken);

        return Response.ok(appDestination).build();
    }

    @PUT
    @Path("/location-point/destination/{destination-id}/status/{status-type-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateDestinationStatus(@PathParam("destination-id") String destinationId,
                                                @PathParam("status-type-id") String statusTypeId,
                                                @HeaderParam("Authorization") String rawToken) throws GeneralAppException {

        AppDestination appDestination = destinationManager.updateDestinationStatus(destinationId, statusTypeId, rawToken);

        return Response.ok(appDestination).build();
    }

    @PUT
    @Path("/location-point/{location-point-id}/status/{status-type-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateLocationPointStatus(@PathParam("location-point-id") String locationPointId,
                                                @PathParam("status-type-id") String statusTypeId,
                                                @HeaderParam("Authorization") String rawToken) throws GeneralAppException {

        AppLocationPoint appLocationPoint = locationPointManager.updateLocationPointStatus(locationPointId, statusTypeId, rawToken);

        return Response.ok(appLocationPoint).build();
    }

    @GET
    @Path("/lookup/status-type")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllStatusTypes(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {

        StatusTypePayload statusTypePayload = lookupManager.getAllStatusTypes(rawToken);

        return Response.ok(statusTypePayload).build();
    }

    @PUT
    @Path("/location-point/location-config/{location-config-id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateLocationPointConfig(@PathParam("location-config-id") String locationConfigId,
                                              @QueryParam("use-qr-code") String useQrCode,
                                              @QueryParam("collect-phone-number") String collectPhoneNumber,
                                              @QueryParam("collect-first-name") String collectFirstName,
                                              @QueryParam("collect-last-name") String collectLastName,
                                              @QueryParam("collect-email") String collectEmail,
                                              @QueryParam("collect-address") String collectAddress,
                                              @QueryParam("ask-for-destination") String askForDestination,
                                              @QueryParam("ask-for-number-of-persons") String askForNumberOfPersons,
                                              @QueryParam("notify-destination-via-email") String notifyDestinationViaEmail,
                                              @QueryParam("notify-destination-via-sms") String notifyDestinationViaSms,
                                              @QueryParam("disable-sign-in-button") String disableSignInButton,
                                              @QueryParam("disable-sign-out-button") String disableSignOutButton,
                                              @HeaderParam("Authorization") String rawToken) throws GeneralAppException {

        AppLocationConfig appLocationConfig = locationConfigManager.updateLocationConfig(locationConfigId, useQrCode, collectPhoneNumber, collectFirstName,
                collectLastName, collectEmail, collectAddress, askForDestination, askForNumberOfPersons, notifyDestinationViaEmail, notifyDestinationViaSms,
                disableSignInButton, disableSignOutButton, rawToken);

        return Response.ok(appLocationConfig).build();
    }
    
}
