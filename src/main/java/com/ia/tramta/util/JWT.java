/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ia.tramta.util;

import com.ia.tramta.pojo.AppUser;
import javax.xml.bind.DatatypeConverter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import javax.ejb.Stateless;
import com.ia.tramta.util.exception.GeneralAppException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.Key;
import java.util.Date;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
public class JWT {    
    SecretKey secretKey = new SecretKey();   
    private final String issuer = "mcore";

    public String createJWT(AppUser user, long ttlMillis) {        
        
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey.getSecret());
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        
        JwtBuilder builder = Jwts.builder()
                                    //.setId(id)
                                    .setIssuedAt(now)
                                    .setSubject(user.getEmail())
                                    .setIssuer(issuer)
                                    .signWith(signatureAlgorithm, signingKey);

        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        return builder.compact();
    }

    
    public Claims parseJWT(String jwt) {
 
        Claims claims = Jwts.parser()         
           .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey.getSecret()))
           .parseClaimsJws(jwt).getBody();
        
        return claims;
    }
       
    public Claims verifyJwt(String rawToken, String resource) throws GeneralAppException {
        try {
            String authToken = rawToken.substring(7);
            return parseJWT(authToken);
        }  catch (Exception e) {
            throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, "Invalid token supplied", "Token supplied could not be parsed",
                resource);
        }
    }
    
}
