/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.util;

import com.ia.tramta.manager.ExceptionThrowerManagerLocal;
import com.ia.tramta.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class Verifier {

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
       
    private String resourceUrl;   
    
    public Verifier setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
        return this;
    }
    
    public Verifier verifyParams(String... params) throws GeneralAppException {
        for (String param : params) {
            if (param == null || param.isEmpty()) {
                exceptionManager.throwNullParametersException(resourceUrl);
            }
        }

        return this;
    }        
    
    public void verifyInteger(String... params) throws GeneralAppException {
        for (String param : params) {
            try {
                Integer.parseInt(param);
            } catch (Exception e) {
                exceptionManager.throwInvalidIntegerAttributeException(resourceUrl);
            }            
        }
    }

    public void verifyInteger(Integer... params) throws GeneralAppException {
        for (Integer param : params) {
            if (param == null) {
                exceptionManager.throwNullParametersException(resourceUrl);
            }
        }
    }

    public void verifyDouble(String... params) throws GeneralAppException {
        for (String param : params) {
            try {
                Double.parseDouble(param);
            } catch (Exception e) {
                exceptionManager.throwInvalidDoubleAttributeException(resourceUrl);
            }            
        }
    }       
    
    public void verifyEmail(String email) throws GeneralAppException {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        
        if (!m.matches()) { 
            exceptionManager.throwInvalidEmailAddressException(resourceUrl);                        
        }
    } 
    
    public Claims verifyJwt(String rawToken) 
            throws GeneralAppException {
        try {
            String authToken = rawToken.substring(7);
            JWT  token = new JWT();  
            return token.parseJWT(authToken);
        }  catch (Exception e) {
            System.out.println("TOKEN EXCEPTION: " + e.getMessage());
            exceptionManager.throwInvalidTokenException(resourceUrl);
        }
        return null;
    }
    
}
