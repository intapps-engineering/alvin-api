package com.ia.tramta.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHandler {
    private final static String dateTimeFormatPattern = "dd MMMM, yyyy";

    public static String formatDate(Date date) {
        final DateFormat formatter = new SimpleDateFormat(dateTimeFormatPattern);
        final String nowString = formatter.format(date);

        return nowString;
    }

}
