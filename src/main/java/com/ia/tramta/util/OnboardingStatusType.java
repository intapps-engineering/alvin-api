/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.util;

/**
 *
 * @author buls
 */
public enum OnboardingStatusType {

    CREATE_USERNAME("CREATE_USERNAME"), UPDATE_PROFILE("UPDATE_PROFILE"), 
    SET_ZAKAT_GOAL("SET_ZAKAT_GOAL"), ADD_PAYMENT_OPTION("ADD_PAYMENT_OPTION"),
    CHOOSE_INTERESTS("CHOOSE_INTERESTS"), STATUS_UNKNOWN("STATUS_UNKNOWN"), COMPLETE("COMPLETE");
    
    String description;

    OnboardingStatusType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    
}
