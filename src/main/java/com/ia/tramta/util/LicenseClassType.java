/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.util;

/**
 *
 * @author buls
 */
public enum LicenseClassType {

    STARTER_TRIAL("STARTER_TRIAL"), STANDARD_TRIAL("STANDARD_TRIAL"),
    PREMIUM_TRIAL("PREMIUM_TRIAL"),
    STARTER("STARTER"), STANDARD("STANDARD"),
    PREMIUM("PREMIUM"), ENTERPRISE("ENTERPRISE");

    String description;

    LicenseClassType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    
}
