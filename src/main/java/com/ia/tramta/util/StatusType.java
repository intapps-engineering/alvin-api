/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.util;

/**
 *
 * @author buls
 */
public enum StatusType {

    ACTIVE("ACTIVE"), INACTIVE("INACTIVE");

    String description;

    StatusType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    public static String getStatusType (String statusTypeId) {
        String status = null;
        if (statusTypeId.equals(StatusType.ACTIVE.getDescription())) {
            status = StatusType.ACTIVE.getDescription();
        } else if (statusTypeId.equals(StatusType.INACTIVE.getDescription())) {
            status = StatusType.INACTIVE.getDescription();
        }

        return status;
    }
    
}
