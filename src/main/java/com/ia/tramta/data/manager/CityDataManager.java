/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.data.provider.DataProviderLocal;
import com.ia.tramta.model.City;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;

@Stateless
public class CityDataManager implements CityDataManagerLocal {

    @EJB
    private DataProviderLocal crud;


    @Override
    public City get(Integer cityId) throws GeneralAppException {
        return crud.find(cityId, City.class);
    }

    @Override
    public List<City> getAll(Integer stateId) throws GeneralAppException {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("stateId", stateId);

        return crud.findByNamedQuery("City.findByStateId", parameters, City.class);
    }
}

