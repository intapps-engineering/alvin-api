/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.data.provider.DataProviderLocal;
import com.ia.tramta.model.Organisation;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class OrganisationDataManager implements OrganisationDataManagerLocal {

    @EJB
    private DataProviderLocal crud;


    @Override
    public Organisation create(Organisation organisation) throws EJBTransactionRolledbackException {
        return crud.create(organisation);
    }

    @Override
    public Organisation update(Organisation organisation) {
        return crud.update(organisation);
    }

    @Override
    public Organisation get(String id) {
        return crud.find(id, Organisation.class);
    }

    @Override
    public void delete(Organisation organisation) {
        crud.delete(organisation);
    }

    @Override
    public Integer getTotalCount() {
        return crud.findAll(Organisation.class).size();
    }
}

