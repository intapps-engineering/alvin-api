/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.data.provider.DataProviderLocal;
import com.ia.tramta.model.Destination;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import java.util.HashMap;

@Stateless
public class DestinationDataManager implements DestinationDataManagerLocal {

    @EJB
    private DataProviderLocal crud;


    @Override
    public Destination create(Destination destination) throws EJBTransactionRolledbackException {
        return crud.create(destination);
    }

    @Override
    public Destination update(Destination destination) {
        return crud.update(destination);
    }

    @Override
    public Destination get(String id) {
        return crud.find(id, Destination.class);
    }

    @Override
    public void delete(Destination destination) {
        crud.delete(destination);
    }

    @Override
    public Integer getTotalDestinationCountInLocation(String locationId) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("locationId", locationId);

        return crud.findByNamedQuery("Destination.findTotalCountByLocationId", parameters, Destination.class).size();
    }
}

