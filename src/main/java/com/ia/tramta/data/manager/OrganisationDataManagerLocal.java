/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.model.Organisation;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;


@Local
public interface OrganisationDataManagerLocal {
    
    Organisation create(Organisation organisation) throws EJBTransactionRolledbackException;

    Organisation update(Organisation organisation);

    Organisation get(String id);

    void delete(Organisation organisation);

    Integer getTotalCount();

}
