/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.model.User;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;


@Local
public interface UserDataManagerLocal {
    
    User create(User user) throws EJBTransactionRolledbackException;

    User update(User user);

    User get(String userId);

    void delete(User user);
    
    List<User> getByEmail(String email);
    
    List<User> getByFacebookEmail(String email);
    
    List<User> getByTwitterEmail(String email);
    
    List<User> getByUsernameFirstNameAndLastName(String searchTerm);

}
