/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.model.State;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.Local;
import java.util.List;


@Local
public interface StateDataManagerLocal {
    
    State get(Integer stateId) throws GeneralAppException;

    List<State> getAll(Integer countryId) throws GeneralAppException;

}
