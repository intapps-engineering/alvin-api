/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.model.LocationPoint;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;


@Local
public interface LocationPointDataManagerLocal {
    
    LocationPoint create(LocationPoint locationPoint) throws EJBTransactionRolledbackException;

    LocationPoint update(LocationPoint locationPoint);

    LocationPoint get(String id);

    void delete(LocationPoint locationPoint);

    Integer getTotalLocationPointCountInOrg(String orgId);

}
