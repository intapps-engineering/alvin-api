/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.model.Destination;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;


@Local
public interface DestinationDataManagerLocal {
    
    Destination create(Destination destination) throws EJBTransactionRolledbackException;

    Destination update(Destination destination);

    Destination get(String id);

    void delete(Destination destination);

    Integer getTotalDestinationCountInLocation(String locationPointId);

}
