/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.model.LocationConfig;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.Local;
import java.util.List;


@Local
public interface LocationConfigDataManagerLocal {
    
    List<LocationConfig> getLocationConfigByLocationPointId(String locationPointId) throws GeneralAppException;

    LocationConfig getLocationConfig(String id) throws GeneralAppException;

    LocationConfig updateLocationConfig(LocationConfig locationConfig) throws GeneralAppException;

    LocationConfig create(LocationConfig locationConfig) throws GeneralAppException;
}
