/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.data.provider.DataProviderLocal;
import com.ia.tramta.model.LocationConfig;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;

@Stateless
public class LocationConfigDataManager implements LocationConfigDataManagerLocal {

    @EJB
    private DataProviderLocal crud;

    @Override
    public List<LocationConfig> getLocationConfigByLocationPointId(String locationPointId) throws GeneralAppException {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("locationPointId", locationPointId);

        return crud.findByNamedQuery("LocationConfig.findByLocationPointId", parameters, LocationConfig.class);
    }

    @Override
    public LocationConfig getLocationConfig(String id) throws GeneralAppException {
        return crud.find(id, LocationConfig.class);
    }

    @Override
    public LocationConfig updateLocationConfig(LocationConfig locationConfig) throws GeneralAppException {
        return crud.update(locationConfig);
    }

    @Override
    public LocationConfig create(LocationConfig locationConfig) throws GeneralAppException {
        return crud.create(locationConfig);
    }
}

