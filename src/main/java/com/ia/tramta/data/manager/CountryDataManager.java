/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.data.provider.DataProviderLocal;
import com.ia.tramta.model.Country;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CountryDataManager implements CountryDataManagerLocal {

    @EJB
    private DataProviderLocal crud;


    @Override
    public Country get(Integer countryId)  throws GeneralAppException {
        return crud.find(countryId, Country.class);
    }

    @Override
    public List<Country> getAll()  throws GeneralAppException {
        return crud.findAll(Country.class);
    }

}

