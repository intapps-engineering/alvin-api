/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.data.provider.DataProviderLocal;
import com.ia.tramta.model.LocationPoint;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import java.util.HashMap;

@Stateless
public class LocationPointDataManager implements LocationPointDataManagerLocal {

    @EJB
    private DataProviderLocal crud;


    @Override
    public LocationPoint create(LocationPoint locationPoint) throws EJBTransactionRolledbackException {
        return crud.create(locationPoint);
    }

    @Override
    public LocationPoint update(LocationPoint locationPoint) {
        return crud.update(locationPoint);
    }

    @Override
    public LocationPoint get(String id) {
        return crud.find(id, LocationPoint.class);
    }

    @Override
    public void delete(LocationPoint locationPoint) {
        crud.delete(locationPoint);
    }

    @Override
    public Integer getTotalLocationPointCountInOrg(String orgId) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("orgId", orgId);

        return crud.findByNamedQuery("LocationPoint.findTotalCountByOrgId", parameters, LocationPoint.class).size();
    }
}

