/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ia.tramta.data.manager;

import com.ia.tramta.data.provider.DataProviderLocal;
import com.ia.tramta.model.State;
import com.ia.tramta.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.HashMap;
import java.util.List;

@Stateless
public class StateDataManager implements StateDataManagerLocal {

    @EJB
    private DataProviderLocal crud;


    @Override
    public State get(Integer stateId) throws GeneralAppException {
        return crud.find(stateId, State.class);
    }

    @Override
    public List<State> getAll(Integer countryId) throws GeneralAppException {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("countryId", countryId);

        return crud.findByNamedQuery("State.findByCountryId", parameters, State.class);
    }
}

