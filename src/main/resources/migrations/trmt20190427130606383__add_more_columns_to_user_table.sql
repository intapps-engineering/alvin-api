ALTER TABLE `user` 
ADD COLUMN `photo_url` VARCHAR(2000) NULL DEFAULT NULL AFTER `password`,
ADD COLUMN `creation_date` DATE NOT NULL AFTER `photo_url`,
ADD COLUMN `facebook_email` VARCHAR(255) NULL DEFAULT NULL AFTER `creation_date`,
ADD COLUMN `twitter_email` VARCHAR(255) NULL DEFAULT NULL AFTER `facebook_email`,
ADD COLUMN `location` VARCHAR(255) NULL DEFAULT NULL AFTER `twitter_email`,
ADD COLUMN `oauth_uid` VARCHAR(100) NULL DEFAULT NULL AFTER `location`,
ADD COLUMN `oauth_provider` VARCHAR(100) NULL DEFAULT NULL AFTER `oauth_uid`,
ADD COLUMN `twitter_id` VARCHAR(100) NULL DEFAULT NULL AFTER `oauth_provider`,
ADD COLUMN `facebook_id` VARCHAR(100) NULL DEFAULT NULL AFTER `twitter_id`,
ADD COLUMN `optout` INT(2) NULL DEFAULT NULL AFTER `facebook_id`;