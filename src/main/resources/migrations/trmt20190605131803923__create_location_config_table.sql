CREATE TABLE `location_config` (
  `id` VARCHAR(45) NOT NULL,
  `location_id` VARCHAR(45) NULL,
  `use_qr_code` TINYINT(1) NULL DEFAULT 0,
  `collect_phone_number` TINYINT(1) NULL DEFAULT 1,
  `collect_first_name` TINYINT(1) NULL DEFAULT 1,
  `collect_last_name` TINYINT(1) NULL DEFAULT 1,
  `collect_email` TINYINT(1) NULL DEFAULT 1,
  `collect_address` TINYINT(1) NULL DEFAULT 1,
  `ask_for_destination` TINYINT(1) NULL DEFAULT 1,
  `ask_for_number_of_persons` TINYINT(1) NULL DEFAULT 1,
  `notify_destination_via_email` TINYINT(1) NULL DEFAULT 0,
  `notify_destination_via_sms` TINYINT(1) NULL DEFAULT 0,
  `disable_sign_in_button` TINYINT(1) NULL DEFAULT 0,
  `disable_sign_out_button` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `loc_config_location_point_id_idx` (`location_id` ASC),
  CONSTRAINT `loc_config_location_point_id`
    FOREIGN KEY (`location_id`)
    REFERENCES `location_point` (`location_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
